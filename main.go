package main

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"goapp/handler"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/joho/godotenv"
)

func init() {
	if err := godotenv.Load(); err != nil {
		// panic(err)
	}
}

func main() {
	port := os.Getenv("APP_PORT")
	appChan := make(chan os.Signal, 1)
	signal.Notify(appChan, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP, syscall.SIGKILL)

	// Fiber instance
	app := fiber.New()
	// Or extend your config for customization
	app.Use(cors.New(cors.Config{
		Next:             nil,
		AllowOrigins:     "*",
		AllowMethods:     "*",
		AllowHeaders:     "*",
		AllowCredentials: true,
		ExposeHeaders:    "",
		MaxAge:           0,
	}))

	apiRoute := app.Group("/api")

	apiRoute.Get("/endpoint1", handler.Endpoint1Handler)
	app.Listen(fmt.Sprintf(":%s", port))
	<-appChan
}
