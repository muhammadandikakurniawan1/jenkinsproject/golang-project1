package handler

import "github.com/gofiber/fiber/v2"

func Endpoint1Handler(fiberCtx *fiber.Ctx) (err error) {
	result := map[string]interface{}{
		"data":    "endpoint1",
		"version": "0.0.1",
	}
	fiberCtx.JSON(result)
	return
}
